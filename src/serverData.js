

export const orderUrl = `http://localhost:3000/order/`;
export const orderIdUrl = `http://localhost:3000/orderid/1`;
export const productUrl = `http://localhost:3000/product/`;
export const itemUrl = `http://localhost:3000/item/`;
export const itemIdUrl = `http://localhost:3000/itemid/1`;
export const logsUrl = `http://localhost:3000/logs`;