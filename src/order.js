import { Item } from './item';
import { getAll } from './getData';
import { itemUrl, productUrl, itemIdUrl, orderIdUrl, orderUrl } from './serverData';
import { Log } from './log';


export class Order {
    constructor(id, itemID, date, purchaser, picture, logId, worker) {
        this.id = id;
        this.itemID = itemID;
        this.purchaser = purchaser;
        this.picture = picture;
        this.items = [];
        this.log = new Log(logId, worker, date);
        this.getItems();
    }

    getItems() {

        if (this.itemID) {
            this.itemID.forEach(id => {
                getAll(`${itemUrl}${id}`).then(item => {
                    this.items.push(new Item(item.id, item.productID, item.quantity));
                });
            });
        }

    }


    drawOrder(container) {
        const orderBox = document.createElement('div');
        orderBox.id = `order${this.id}`;
        orderBox.className = 'item border border-secondary';
        orderBox.innerHTML =
            `<div class='text-center'><img class='img ' src='${this.picture}'></div>` +
            `<div class='card-body'>` +
            `<div class='text-center'><h4 class='card-title'>${this.id}. Narucilac: ${this.purchaser}</h4></div>` +
            `<p class='card-text'>Datum: ${this.log.getDate()}</p>` +
            `<button id='btnOpen' value='${this.id}' type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal'>Pregledaj</button>` +
            `<button id='btnDeleteOrder' value='${this.id}' type='button' class='btn btn-primary'>Obriši</button>` +
            '</div>';
        container.appendChild(orderBox);
    }

    draworderEdit(id) {
        const modal = document.getElementById('order');
        modal.innerHTML = this.drawOrderInfo(id);
        if (!id) {
            this.items.map(item => {
                item.drawItem(this.id);
            });
            this.drawAddBlock();
        }
    }

    drawOrderInfo(id) {
        let innerHtml = `<label>Id narudžbine</label>`;
        if (!id) {
            innerHtml += `<input type='text' class='form-control' id='orderId' value='${this.id}'disabled>` +
                `<label>Naručilac</label>` +
                `<input type='text' class='form-control' id='purchaser' value='${this.purchaser}' disabled>` +
                `<label>Radnik</label>` +
                `<input type='text' class='form-control' id='worker' value='${this.log.getWorker()}' disabled >` +
                `<label>Datum</label>` +
                `<input type='date' class='form-control datetime-local' id='orderId'  value='${this.log.getDate()}' disabled>` +
                `<div id='items'>` +
                `<h5>Stavke</h5>`;
        }
        else {
            innerHtml += `<input type='text' class='form-control' id='orderId' value='${id}' disabled>` +
                `<label>Naručilac</label>` +
                `<input type='text' class='form-control' id='purchaser' >` +
                `<label>Radnik</label>` +
                `<input type='text' class='form-control' id='worker' >` +
                `<label>Datum</label>` +
                `<input type='date' class='form-control datetime-local' id='date' value='2019-01-01' >` +
                `<button id='submitOrder' value='${id}' type="button" class="btn btn-primary" >Dodaj</button>`;
        }
        innerHtml += `</div>`;
        return innerHtml;
    }

    drawAddBlock() {
        let div = document.createElement('div');
        div.id = "addItem";

        let select = document.createElement('select');
        select.className = "custom-select";
        select.id = "productSelect";
        getAll(productUrl).then(products => {
            products.forEach(product => {
                let option = document.createElement('option');
                option.value = product.id;
                option.text = `${product.name} - ${product.price} din`;
                select.appendChild(option);
            })
        });

        let heder = document.createElement('h5');
        heder.innerHTML = "Dodaj Stavku";
        div.appendChild(heder);
        div.appendChild(select);

        let input = document.createElement('input');
        input.className = 'form-control';
        input.placeholder = 'Količina';
        input.id = "inputQuantity"
        div.appendChild(input);

        let button = document.createElement('button');
        button.id = "addItem";
        button.value = this.id;
        button.className = 'btn btn-primary';
        button.innerHTML = 'Dodaj';
        div.appendChild(button);

        document.getElementById('order').appendChild(div);
    }

    delItem(id) {
        let del = this.itemID.indexOf(parseInt(id));
        this.itemID.splice(del, 1);
        this.items[del].deleteItemFromDB();
        this.items.splice(del, 1);
        this.EditOrderDB();
        let parent = document.getElementById('items');
        let child = document.getElementById(`item${id}`);
        parent.removeChild(child);
    }


    async addItem() {
        const select = document.getElementById("productSelect");
        let productId = select.options[select.selectedIndex].value;
        let quantity = document.getElementById('inputQuantity').value;
        if (quantity && productId) {
            let lastId;
            await getAll(itemIdUrl).then(data => {
                lastId = data.lastID;
            });
            lastId++;
            this.itemID.push(lastId);

            let item = new Item(lastId, productId, quantity);
            await item.getProduct();


            Item.EditItemIDDB(lastId);
            await item.addItemToDB();
            await this.EditOrderDB();
            this.items.push(item);
            await item.drawItem(this.id);
        }
    }

    EditOrderDB() {
        fetch(`${orderUrl}${this.id}`,
            {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: this.id,
                    itemID: this.itemID,
                    date: this.date,
                    purchaser: this.purchaser,
                    picture: this.picture
                })
            }).then()
            .catch((error) => {
                console.log(error);
            })
    }

    static EditOrderIDDB(lastID) {
        fetch(orderIdUrl,
            {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: 1,
                    lastID: lastID
                })

            }).then()
            .catch((error) => {
                console.log(error);
            })
    }

    static delOrderFromDB(id) {
        fetch(`${orderUrl}${id}`,
            {
                method: 'delete',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then()
            .catch((error) => {
                console.log(error)
            })
    }

}
