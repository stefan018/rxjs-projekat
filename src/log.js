
export class Log {
    constructor(id, worker, date) {
        this.id = id;
        this.worker = worker;
        this.date = date;
    }

    getLogId() {
        return this.id;
    }

    getWorker() {
        return this.worker;
    }

    getDate() {
        return this.date;
    }
}