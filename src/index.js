import { OrderService } from './orderService';
import { fromEvent, zip } from "rxjs";
import { debounceTime, tap, filter, map, switchMap, pluck } from "rxjs/operators";
import { Order } from './order';



const data$ = zip(
    OrderService.fetchOrders(),
    OrderService.fetchLogs(),
    ((orders, logs) => { return { orders, logs } })
)
    .subscribe((data) => {
        for (let i = 0; i < data.orders.length; i++) {
            OrderService.orders.push(new Order(data.orders[i].id, data.orders[i].itemID, data.logs[i].date,
                data.orders[i].purchaser, data.orders[i].picture, data.logs[i].id, data.logs[i].worker))
        }
    });

setTimeout(() => {
    OrderService.drawOrders();
}, 1000);



const searchOrder$ = fromEvent(document.getElementById('searchOrder'), 'input').pipe(
    debounceTime(1000),
    switchMap(
        event => {
            return OrderService.searchOrder(event.target.value)
        }
    )).subscribe(orders => {
        const container = document.getElementById('container');
        container.innerHTML = "";
        orders.forEach(order => {
            order.drawOrder(container);
        });
    })


const btnOpenClick$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('btnOpen')),
    pluck('target', 'value'),
    debounceTime(1000),
    switchMap(id => {
        return OrderService.findOrderForEdit(id)
    })
);
btnOpenClick$.subscribe(order => {
    order.draworderEdit();
});

const btnDelPro$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('btnDelPro')),
    pluck('target', 'value'),
    debounceTime(1000),
    tap(id => OrderService.delItem(id))
);
btnDelPro$.subscribe();

const btnAddItem$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('addItem')),
    pluck('target', 'value'),
    debounceTime(1000),
    tap(id => OrderService.addItem(id))
);
btnAddItem$.subscribe();

const btnAddOrder$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('addOrder')),
    tap(() => OrderService.drawNewOrderDialog())
);
btnAddOrder$.subscribe();


const submitOrder$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('submitOrder')),
    pluck('target', 'value'),
    debounceTime(1000),
    tap(id => OrderService.submitOrder())
);
submitOrder$.subscribe();

const delOrder$ = fromEvent(document, 'click').pipe(
    filter(event => event.target.id.includes('btnDeleteOrder')),
    map(event => event.target.value),
    debounceTime(1000),
    tap(id => OrderService.delOrder(id))
);
delOrder$.subscribe();










