

export class Product {
    constructor(id, name, price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    getProductId() {
        return this.id;
    }

    getPrice() {
        return this.price;
    }
    getName() {
        return this.name;
    }


}