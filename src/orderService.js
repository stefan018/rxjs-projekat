import { Order } from './order';
import { getAll } from './getData';
import { of, from } from 'rxjs';
import { orderIdUrl, orderUrl, logsUrl } from './serverData';



let orders = [];
export class OrderService {
    static get orders() { return orders; }
    static set orders(value) { orders = value };

    static fetchOrders() {
        try {
            const response = getAll(orderUrl);
            return from(response);
        }
        catch (err) {
            console.log(err);
        }

    }

    static fetchLogs() {
        try {
            const response = getAll(logsUrl);
            return from(response);

        }
        catch (err) {
            console.log(err);
        }

    }



    static drawOrders() {

        const container = document.getElementById('container');
        container.innerHTML = "";
        OrderService.orders.forEach(order => {
            order.drawOrder(container);
        });
    }

    static searchOrder(text) {
        const container = document.getElementById('container');
        container.innerHTML = "";
        const orders = OrderService.orders.filter(order => {
            if (order.purchaser.toLowerCase().includes(text.toLowerCase()) == true)
                return order;
        });
        return of(orders);

    }

    static findOrderForEdit(id) {
        if (id) {
            const order = OrderService.orders.filter(order => order.id === id)[0];
            return of(order);
        }
    }

    static async drawNewOrderDialog() {

        let order = new Order();
        let lastId;
        lastId = (await this.getLastID());
        order.draworderEdit(lastId);
    }


    static delItem(id) {
        let ids = id.split('-');
        OrderService.orders.filter(order => order.id === ids[1])[0].delItem(ids[0]);
    }

    static addItem(id) {
        if (OrderService.orders) {
            OrderService.orders.filter(order => order.id === id)[0].addItem();
        }

    }

    static async getLastID() {
        let lastId;
        await getAll(orderIdUrl).then(data => {
            lastId = data.lastID;
        });
        lastId++;
        return lastId;
    }

    static submitOrder() {
        let id = document.getElementById('orderId').value;
        let purchaser = document.getElementById('purchaser').value;
        let date = document.getElementById('date').value;
        let worker = document.getElementById('worker').value;
        if (id && purchaser && date && worker) {
            let order = new Order(id, [], date, purchaser, `src/img/${purchaser}.jpg`, id, worker);
            OrderService.orders.push(order);
            order.draworderEdit();
            OrderService.addOrderToDB(order);
            OrderService.addLogToDb(date, worker, id);
            Order.EditOrderIDDB(parseInt(id));
            const container = document.getElementById('container');
            order.drawOrder(container);

        }
    }

    static addLogToDb(date, worker, id) {
        fetch(logsUrl,
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: id,
                    date: date,
                    worker: worker
                })
            }).then()
            .catch((error) => {
                console.log(error);
            })
    }
    
    static addOrderToDB(order) {
        fetch(orderUrl,
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: order.id,
                    itemID: order.itemID,
                    date: order.date,
                    purchaser: order.purchaser,
                    picture: order.picture
                })
            }).then()
            .catch((error) => {
                console.log(error);
            })
    }

    static delOrder(id) {
        let parent = document.getElementById('container');
        let child = document.getElementById(`order${id}`);
        parent.removeChild(child);
        Order.delOrderFromDB(id);
        OrderService.orders = OrderService.orders.filter(order => order.id != id);


    }


}
