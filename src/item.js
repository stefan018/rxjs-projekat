import { Product } from './product'
import { getAll } from './getData'
import { itemIdUrl, productUrl, itemUrl } from './serverData';

export class Item {
    constructor(id, productID, quantity) {
        this.id = id;
        this.productID = productID;
        this.quantity = quantity;
        this.product;
        this.getProduct();

    }

    async getProduct() {
        await getAll(`${productUrl}${this.productID}`).then(data => {
            this.product = new Product(data.id, data.name, data.price);
        });
    }



    drawItem(id) {
        let innerHTML = `<h6>Proizvod:</h6>` +
            `<label> ${this.product.getName()} - </label><lable>${this.quantity} kom - </label><lable> ${this.product.getPrice()} din</label><button id='btnDelPro' value='${this.id}-${id}' type='button' class='btn btn-light'>Obriši</button><br/>`;
        let div = document.createElement('div');
        div.id = `item${this.id}`;
        div.innerHTML += innerHTML;

        document.getElementById('items').appendChild(div);
    }

    addItemToDB() {
        fetch(itemUrl,
            {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: this.id,
                    productID: this.productID,
                    quantity: this.quantity
                })

            }).then()
            .catch((error) => {
                console.log(error)
            })
    }

    deleteItemFromDB() {
        fetch(`${itemUrl}${this.id}`,
            {
                method: 'delete',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then()
            .catch((error) => {
                console.log(error)
            })
    }

    static EditItemIDDB(lastID) {
        fetch(itemIdUrl,
            {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: 1,
                    lastID: lastID
                })

            }).then()
            .catch((error) => {
                console.log(error);
            })
    }

}